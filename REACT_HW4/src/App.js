import './App.css';
import "./Styles.scss";
import "./components/Modal/ModalWindow.scss"
import React, {useEffect} from 'react';
import Header from "./components/Header/Header";
import Footer from "./components/Footer/Footer";
import BodyHead from "./components/BodyHead/BodyHead";
import AppRoutes from "./routes/AppRoutes";


function App() {

    const getArray = JSON.parse(localStorage.getItem('FAVORITES LIST') || 0);

    useEffect(() => {
        if (getArray !== 0) {
        }
    }, [getArray]);


    return (<>
            <Header/>
            <BodyHead/>
            <AppRoutes/>
            <Footer/>
        </>
    );
}

export default App;
