import React from 'react';
import {useHistory} from "react-router-dom"

function Page404(props) {

    const history = useHistory();

    const goToHome = () => {
        history.push('/')

    };

    return (
        <div><h2>Error 404</h2>
            <h4>Page not found</h4>
            <div>
                <button onClick={goToHome}>Go to home page</button>
            </div>
        </div>

    );
}

export default Page404;