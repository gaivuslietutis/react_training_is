import React from 'react';


function Button(props) {

    const {text, onClick, textModal, styles} = props;

    return (
        <>
            <button className="buttonBuy" style={{...styles}} onClick={onClick}
                    data-text={textModal}>{text}</button>
        </>
    );
}

    export default Button;