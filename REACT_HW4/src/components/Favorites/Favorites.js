import React from 'react';
import './favoritesStyle.scss'

function Favorites(props) {
    const {favorites} = props;

    return (
        <div className='block row center'>

            <h2>Favorites</h2>
            <div className='row center'>
            <div>{favorites.length === 0 && <div>Add Favorites</div>}</div>
            <div className='row center col-2'>
                {favorites.map(item => <div key={item.id}><img className='medium ' src={item.image} alt="img"/></div>)}
            </div>
            </div>
        </div>
    );
}

export default Favorites;