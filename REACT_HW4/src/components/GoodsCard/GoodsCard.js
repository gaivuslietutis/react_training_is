import React, {useState} from 'react';
import Button from "../Button/Button";
import PropTypes from "prop-types"
import GoodsItem from "../GoodsItem/GoodsItem";
import Modal from "../Modal/Modal";
import {ACTION_BUTTON_STYLES} from '../Variables/modalStyles.js';
import {HEADER, close, CLOSE_BUTTON, TEXT_FIRST_MODAL} from '../Variables/modalStyles.js';
import {FaStar, FaRegStar} from 'react-icons/fa';
import {connect} from "react-redux";
import {addToCart, toggleFavorites} from "../../redux/Shopping/shopping-actions";

function GoodsCard(props) {

    const [open, setOpen] = useState(false);

    const openModal = (e) => {
        setOpen(true);
    };

    const closeModal = () => {
        setOpen(false)
    };

    const {good, buttonBuy} = props;

    const {addToCart, toggleFavorites, favoriteItems} = props;

    return (
        <>
            <div className="goods__card">
                <GoodsItem good={good}/>
                <Button text='Add to cart'
                        onClick={openModal}
                        good={good}
                        buttonBuy={buttonBuy}/>
                {favoriteItems.includes(good) ? (<FaStar
                        onClick={
                            () => toggleFavorites(good)
                        }
                        style={{color: "red", width: 36, height: 36}}
                    />
                ) : (<FaRegStar
                    onClick={
                        () => toggleFavorites(good)
                    }
                    style={{color: "red", width: 36, height: 36}}
                />)}
            </div>

            <Modal
                open={open}
                closeModal={closeModal}
                header={HEADER}
                closeButton={CLOSE_BUTTON}
                close={close}
                text={TEXT_FIRST_MODAL}
                actions={<div>
                    <button onClick={() => {
                        addToCart(good);
                        closeModal()
                    }
                    } style={ACTION_BUTTON_STYLES}>OK
                    </button>
                    <button
                        onClick={closeModal}
                        style={ACTION_BUTTON_STYLES}>Cancel
                    </button>
                </div>}/>
        </>
    );
}


const mapStateToProps = (state) => {
    return {
        favoriteItems: state.shop.favoriteItems,
    }
};


const mapDispatchToProps = (dispatch) => {
    return {
        addToCart: (id) => dispatch(addToCart(id)),
        toggleFavorites: (id) => dispatch(toggleFavorites(id)),
    }
};


GoodsCard.propTypes = {
    good: PropTypes.object
};

export default connect(mapStateToProps, mapDispatchToProps)(GoodsCard);