export const ACTION_BUTTON_STYLES = {
    width: "64px",
    padding: "8px",
    backgroundColor: "darkred",
    marginRight: "8px",
    marginLeft: "8px",
    fontSize: "16px",
    textAlign: "center",
    color: "white",
    border: "1px darkred",
    borderRadius: "4px",
};

export const HEADER = "Please confirm your selection?";
export const close = "X";
export const CLOSE_BUTTON = Boolean(close);
export const TEXT_FIRST_MODAL = "Would you like to select this item? If yes, please do not forget to hit the button 'OK' to add it to your cart?";

export const TEXT_CART_MODAL = "Are you sure to delete this item from your cart?";

