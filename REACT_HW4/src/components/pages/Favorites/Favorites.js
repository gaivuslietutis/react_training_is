import React from 'react';
import './favoritesStyle.scss';

function Favorites(props) {

    const getArray = JSON.parse(localStorage.getItem('FAVORITES LIST') || 0);

    return (
        <div className='block row center'>
            <h3>YOUR FAVORITES</h3>
            <div className='row center'>
                    <div>{Object.keys(getArray[0]).length === 0 ?

                    <div>Add Favorites</div>:

                    <div className='row center col-2'>
                        {getArray.map((item, index) => (
                            <div key={index}><img className='medium' src={item.image} alt='img'/>
                            </div>)
                        )}
                    </div>
                }</div>
            </div>
        </div>
    );
}

export default Favorites;