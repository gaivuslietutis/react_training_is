import * as actionTypes from './user-types'

const INITIAL_STATE={
    user: {
        data: null,
        isLoading: true,
    },
};

const userReducer=(state=INITIAL_STATE, action)=>{
    switch (action.type){
        case actionTypes.SET_USER: {
            return {...state, user: {...state.user, data: action.payload, isLoading: false}}
        }
        default:
            return state
    }

};

export default userReducer;