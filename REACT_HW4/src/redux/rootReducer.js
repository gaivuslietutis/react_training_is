import {combineReducers} from "redux";

import shopReducer from './Shopping/shopping-reducer';
import productFetchReducer from './Products_Fetch/products-fetch-reducer';
import userReducer from "./User_Set/user-reducer";
import modalReducer from "./Modal/modal-reducer";

const rootReducer = combineReducers({
    shop: shopReducer,
    products: productFetchReducer,
    users: userReducer,
    modal: modalReducer
});

export default rootReducer;