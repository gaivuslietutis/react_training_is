import * as actionTypes from './products-fetch-types';

const INITIAL_STATE={

    goods:{
        data:[],
        isLoading:true,
    },

};

const productsFetchReducer=(state=INITIAL_STATE, action)=>{
    switch (action.type){
        case actionTypes.SET_GOODS: {
            return {...state, goods: {...state.goods, data: action.payload, isLoading: false}}
        }
        default:
            return state

    }
};
export default productsFetchReducer;