import * as actionTypes from './modal-types'

export const modalOpen=(newState)=>{
    return{
        type:actionTypes.MODAL_OPEN,
        payload:newState
    }
};

export const modalClose=(newState)=>{
    return{
        type:actionTypes.MODAL_CLOSE,
        payload:newState
    }
};

