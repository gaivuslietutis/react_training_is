import * as actionTypes from './shopping-types';


export const addToCart = (itemID) => (dispatch, getState) => {
    dispatch({
        type: actionTypes.ADD_TO_CART,
        payload: itemID

    });
    localStorage.setItem('PRODUCT SHOP', JSON.stringify(getState().shop.cartItems))
};


export const removeFromCart = (itemID) => (dispatch, getState) => {
    dispatch({
        type: actionTypes.REMOVE_FROM_CART,
        payload: itemID
    });
    localStorage.setItem('PRODUCT SHOP', JSON.stringify(getState().shop.cartItems))
};

export const toggleFavorites = (itemID) => (dispatch, getState)=>{
    dispatch ({
        type: actionTypes.TOGGLE_FAVORITES,
        payload: itemID
    });

   console.log(getState().shop.favoriteItems)

    localStorage.setItem("FAVORITES LIST", JSON.stringify(getState().shop.favoriteItems));
        let storage = localStorage.getItem('FAVORITES PRODUCT' + itemID.id || '0');
        if (storage === null) {
            localStorage.setItem('FAVORITES PRODUCT' + (itemID.id), JSON.stringify(itemID))
        } else {
            localStorage.removeItem('FAVORITES PRODUCT' + (itemID.id))
        }
};

// export const adjustQty=(itemID, value)=>{
//     return{
//         type:actionTypes.ADJUST_QTY,
//         payload:{
//             id:itemID,
//             qty: value
//         }
//     }
// }
//
// export const loadCurrentItem=(item)=>{
//     return{
//         type:actionTypes.LOAD_CURRENT_ITEM,
//         payload:item
//     }
// }

// export const addToCart=(itemID)=>{
//     return{
//         type:actionTypes.ADD_TO_CART,
//         payload: itemID
//     }
// };