export {star} from './star.jsx';
export {mail} from './mail.jsx';
export {facebook} from './facebook.jsx';
export {instagram} from './instagram';
export {phone} from './phone.jsx';
export {artGallery} from './art_galery';
export {art} from './art_icon'
export {remove} from './remove_icon.jsx'