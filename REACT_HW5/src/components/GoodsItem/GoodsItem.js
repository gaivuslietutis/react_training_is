import React from 'react';

function GoodsItem(props) {

    const {good} = props;

    return (
        <div className='goods__item'>
            <h3>{good.title}</h3>
            <img src={good.image} alt="img"/>
            <div>PRICE: {good.price} EUR</div>
            <div>ARTICLE:{good.article}</div>
            <div>COLOR:{good.color}</div>
        </div>
    );
}

export default GoodsItem;