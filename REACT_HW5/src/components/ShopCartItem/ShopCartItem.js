import React from 'react';
import Icon from "../Icon/Icon";
import '../ShoppingCart/shoppingCartStyles.scss'


function ShopCartItem(props) {

    const {shopItem, openModal} = props;
    return (
        <>
            <div className='row center'>
                <img className='small' src={shopItem.image} alt="img"/>
                <div className='col-1 text-left'>{shopItem.title}</div>
                <div className='row center'>
                    <div>
                        {shopItem.qty} x EUR {shopItem.price}
                    </div>
                    <Icon type='remove' onClick={openModal}/>
                </div>
            </div>
            <hr/>

        </>
    );
}

export default ShopCartItem;