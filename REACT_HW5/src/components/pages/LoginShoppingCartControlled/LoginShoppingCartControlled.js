import React, {useEffect, useState} from 'react'
import './loginStylesCart.scss'
import {connect} from "react-redux";
import {handleChange} from "../../../redux/User_Set/user-actions";

const EMAIL_REGEX = /^[a-zA-Z0-9.!#$%&'*+=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;
const MOBILE_REGEX=/^(\+\d{1,3}[- ]?)?\d{10}$/;

function LoginShoppingCartControlled(props) {

    const {values, cartItems} = props;

    const cartItemNotExists = cartItems.length === 0;

    const [errors, setErrors] = useState({});
    const [touched, setTouched] = useState({});
    const [success, setSuccess] = useState(false);

    const validateForm = () => {
        const {login, name, surname, age, address, mobile} = values;
        const errors = {};

        if (!EMAIL_REGEX.test(login)) {
            errors.login = 'This is not a valid email'
        }
        if (!login) {
            errors.login = 'This field is required'
        }
        if (!name) {
            errors.name = 'This field is required'
        }
        if (!surname) {
            errors.surname = 'This field is required'
        }
        if (!age) {
            errors.age = 'This field is required'
        }
        if(isNaN(age)){
            errors.age="This field should be a number"
        }
        if (!address) {
            errors.address = 'This field is required'
        }
        if (!mobile) {
            errors.mobile = 'This field is required'
        }
        if (!MOBILE_REGEX.test(mobile)) {
            errors.mobile = 'This is not a correct mobile'
        }
        setErrors(errors);
        return Object.keys(errors).length === 0
    };

    const register = (e) => {
        e.preventDefault();

        setTouched({login: true, name: true, surname: true, age: true, address: true, mobile: true});

        const isValid = validateForm();

        setSuccess(isValid);

        if (!isValid) {
            return
        }
        console.log(Object.values(values))
    };

    const onHandleChange = ({target}) => {
        props.handleChange(target);
    };

    const handleTouched = (e) => {
        setTouched({...touched, [e.target.name]: true})
    };

    useEffect(() => {
        validateForm()
    }, [values]);

    return (
        <>{!cartItemNotExists &&
            <form className='form-container' onSubmit={register} noValidate>
                {success ? <h3 style={{color: 'red'}}>YOUR FORM HAS BEEN SUCCESSFULLY VALIDATED !!!</h3> :
                    <h3 className='form-page'>FILL IN THE FORM TO MAKE AN ORDER</h3>}
                <div><input name='login' type='text' placeholder='Email: admin@test.com' value={values.login} onChange={onHandleChange} onBlur={handleTouched}/>
                    {errors.login && touched.login && <span className='error'>{errors.login}</span>}
                </div>
                <div>
                    <input name='name' type='text' placeholder='Your name' value={values.name} onChange={onHandleChange} onBlur={handleTouched}/>
                    {errors.name && touched.name && <span className='error'>{errors.name}</span>}
                </div>
                <div><input name='surname' type='text' placeholder='Your surname' value={values.surname} onChange={onHandleChange} onBlur={handleTouched}/>
                    {errors.surname && touched.surname && <span className='error'>{errors.surname}</span>}
                </div>
                <div><input name='age' type='text' placeholder='Your age' value={values.age} onChange={onHandleChange} onBlur={handleTouched}/>
                    {errors.age && touched.age && <span className='error'>{errors.age}</span>}
                </div>
                <div><input name='address' type='text' placeholder='Your address' value={values.address} onChange={onHandleChange} onBlur={handleTouched}/>
                    {errors.address && touched.address && <span className='error'>{errors.address}</span>}
                </div>
                <div><input name='mobile' type='text' placeholder='Your mobile' value={values.mobile} onChange={onHandleChange} onBlur={handleTouched}/>
                    {errors.mobile && touched.mobile && <span className='error'>{errors.mobile}</span>}
                </div>
                <div><button className='login-button' type='submit'>SUBMIT</button></div>
            </form>}
        </>
    )
}

const mapStateToProps = (state) => {
    return {
        values: state.users.values,
        cartItems:state.shop.cartItems
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        handleChange: (e) => dispatch(handleChange(e))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(LoginShoppingCartControlled);
