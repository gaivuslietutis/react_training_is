import React, {useEffect} from 'react';
import GoodsCard from "../../GoodsCard/GoodsCard";
import {connect} from "react-redux";
import {getGoods} from "../../../redux/Products_Fetch/shopping-operations";


function Main(props) {

    const {getGoods, goods} = props;

    useEffect(() => {
        getGoods()
    }, [getGoods]);

    const goodsItems = goods.map(g => <GoodsCard key={g.id} good={g}/>);

    return (
        <>
            <div className="goods__wrapper">{goodsItems}</div>
        </>
    );
}

const mapStateToProps = (state) => {
    return {
        goods: state.products.goods.data
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        getGoods: () => dispatch(getGoods())
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Main);