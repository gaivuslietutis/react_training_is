import React, {useRef} from 'react'
import {Redirect, useHistory} from 'react-router-dom';
import './loginStyles.scss'
import {connect} from "react-redux";

function Login({setUser, isAuth}) {
    const loginRef = useRef(null);
    const passwordRef = useRef(null);
    const history = useHistory();

    const logInUser = (e) => {
        e.preventDefault()

        setUser({
            login: loginRef.current.value,
            password: passwordRef.current.value
        })
        history.push('/')
    }

    if (isAuth) {
        return <Redirect to='/'/>
    }

    return (
        <div>
            <h3 className='form-page'>YOU MUST SIGN IN TO ENTER YOUR SHOPPING CART</h3>
            <form className='form-container' onSubmit={logInUser}>
                <div>
                    <input type='text' placeholder='Email' ref={loginRef}/>
                </div>
                <div>
                    <input type='password' placeholder='Password' ref={passwordRef}/>
                </div>
                <div>
                    <button className='login-button' type='submit'>Log in</button>
                </div>
            </form>
        </div>
    )
}

const mapDispatchToProps = (dispatch) => {
    return {
        setUser: (user) => dispatch({type: 'SET_USER', payload: user})
    }
};

export default connect(null, mapDispatchToProps)(Login);
