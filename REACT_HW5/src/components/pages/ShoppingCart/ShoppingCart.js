import React from 'react';
import './shoppingCartStyles.scss'
import {ACTION_BUTTON_STYLES} from '../../Variables/modalStyles.js'
import Modal from "../../Modal/Modal";
import ShopCartItem from "../../ShopCartItem/ShopCartItem";
import ShoppingCartTotal from "../../ShoppingCartTotal/ShoppingCartTotal";
import {HEADER, close, CLOSE_BUTTON, TEXT_CART_MODAL} from '../../Variables/modalStyles';
import {connect} from "react-redux";
import {removeFromCart} from "../../../redux/Shopping/shopping-actions";
import {modalClose, modalOpen} from "../../../redux/Modal/modal-actions";
import LoginShoppingCartControlled from "../LoginShoppingCartControlled/LoginShoppingCartControlled";


function ShoppingCart(props) {
   const {setUser, user, removeFromCart, cartItems, modalClose, modalOpen, open} = props;

    const logOutUser = () => {
        setUser(null);
        localStorage.removeItem('user')
    };

    const cartItemNotExists = cartItems.length === 0;

    const cartItemsList = cartItems.map(item => <div key={item.id}><ShopCartItem shopItem={item}

                                                                                 openModal={modalOpen}/>
        <Modal open={open} closeModal={modalClose} header={HEADER} closeButton={CLOSE_BUTTON}
               close={close}
               text={TEXT_CART_MODAL}
               actions={<div>
                   <button onClick={() => {
                     removeFromCart(item);
                     modalClose()
                   }} style={ACTION_BUTTON_STYLES}>OK
                   </button>
                   <button onClick={modalClose} style={ACTION_BUTTON_STYLES}>Cancel</button>
               </div>}/>
    </div>);


    return (
        <>
            <div className='block row center'>
                {user && <h3>Welcome to your shopping cart, dear {user.login} !</h3>}
                <div>
                    <button className='shoppingcart-signout' onClick={logOutUser}>Sign out</button>
                </div>
            </div>
            <div className='block row center'>
                {cartItemNotExists && <h3>Shopping Cart:</h3>}
                {!cartItemNotExists && <div>{cartItemsList}</div>}
                {!cartItemNotExists && <div><ShoppingCartTotal cartItems={cartItems}/></div>}
                <div>
                    {cartItemNotExists && <h3 className='cart-title'>Your Cart is empty !</h3>}
                </div>
            </div>
            <LoginShoppingCartControlled/>
        </>
    );
}

const mapStateToProps = (state) => {
    return {
        user: state.users.user.data,
        cartItems: state.shop.cartItems,
        open:state.modal.open
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        setUser: (user) => dispatch({type: 'SET_USER', payload: user}),
        removeFromCart:(itemID)=>dispatch(removeFromCart(itemID)),
        modalClose:(newState)=>dispatch(modalClose(newState)),
        modalOpen:(newState)=>dispatch(modalOpen(newState))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(ShoppingCart);