import React from 'react';
import Icon from "../Icon/Icon";
import './contactBlockStyle.scss'

function ContactIconsBlock(props) {

    return (
        <div className='contact-items'>
            <Icon type='facebook'/>
            <Icon type='mail'/>
            <Icon type='phone'/>
            <Icon type='instagram'/>
        </div>
    );
}

export default ContactIconsBlock;