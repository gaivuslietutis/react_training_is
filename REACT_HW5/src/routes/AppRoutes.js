import React from 'react';
import {Redirect, Route, Switch} from "react-router-dom";
import Favorites from "../components/pages/Favorites/Favorites";
import Main from "../components/pages/Main/Main";
import ShoppingCart from "../components/pages/ShoppingCart/ShoppingCart";
import Page404 from "../components/Page404/Page404";
import Login from "../components/pages/Login/Login";
import {connect} from "react-redux";

function AppRoutes(props) {

    const {user} = props;
    const isAuth = !!user;

    return (
        <Switch>
            {!isAuth && <Redirect exact from='/' to='/main'/>}
            {isAuth && <Redirect exact from='/' to='/cart'/>}
            <Route exact path='/login'><Login isAuth={isAuth}/></Route>
            <Route exact path='/main' render={() => <Main/>}/>
            <Route exact path='/favorites' render={() => <Favorites/>}/>
            <ProtectedRoute exact path='/cart' isAuth={isAuth}>
                <ShoppingCart/>
            </ProtectedRoute>
            <Route exact path='*'><Page404/></Route>
        </Switch>
    );
}

const ProtectedRoute = ({children, isAuth, ...rest}) => {
    return <Route {...rest} render={() => {
        if (isAuth) {
            return children
        } else {
            return <Redirect to='/login'/>
        }
    }}/>
};

const mapStateToProps = (state) => {
    return {
        user: state.users.user.data,
        goods: state.products.goods.data,
        cartItems: state.shop.cartItems
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        setUser: (user) => dispatch({type: 'SET_USER', payload: user})
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(AppRoutes);