import * as actionTypes from './user-types'

const INITIAL_STATE = {
    user: {
        data: null,
        isLoading: true,
    },
    userOrder: {
        data: null,
        isLoading: true,
    },
    values: {
        login: '',
        name: '',
        surname: '',
        age: '',
        address: '',
        mobile: ''
    }
};

const userReducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case actionTypes.SET_USER: {
            return {...state, user: {...state.user, data: action.payload, isLoading: false}}
        }

        case 'SET_VALUES': {
            return {
                ...state, values: {...state.values, [action.payload.name]: action.payload.value}
            }
        }

        default:
            return state
    }

};

export default userReducer;