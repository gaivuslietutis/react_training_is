import * as actionTypes from './user-types';

export const handleChange = (e) => (dispatch, getState) => {
    dispatch({
        type: actionTypes.SET_VALUES,
        payload: e
    });
    localStorage.setItem("USER DATA ORDER", JSON.stringify(getState().users.values))
};
