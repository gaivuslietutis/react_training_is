import * as actionTypes from './modal-types';


const INITIAL_STATE = {
    open: false,
    openShop:false
};

const modalReducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {

        case actionTypes.MODAL_OPEN: {
            return {open:true}
        }

        case actionTypes.MODAL_CLOSE: {
            return {open:false}
        }

        default:
            return state
    }
};


export default modalReducer;

