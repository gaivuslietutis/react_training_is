import React, {Component} from 'react';

class Button extends Component {

    render() {
        const {text, backgroundColor, onClick, textModal, styles} = this.props;
        return (
            <>
                <button style={{...backgroundColor, ...styles}} onClick={onClick} data-text={textModal}>{text}</button>
            </>
        );
    }
}

export default Button;