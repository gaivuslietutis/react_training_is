import React, {Component, Fragment} from 'react';

class Modal extends Component {
    render() {
        const {header, closeButton, close, text, actions, open, closeModal} = this.props;

        if (!open) return null;

        return (
            <>
                <div className='modal-overlay' onClick={closeModal}/>
                <div className='modal-window'>
                    <div className='modal-window__header'>
                        <div>{header}</div>
                        {closeButton && <span onClick={closeModal}>{close}</span>}</div>
                    <div className='modal-window__body'>
                        <div>{text}</div>
                        <div className='action-wrapper'>
                            {actions}</div>
                    </div>
                </div>
            </>
        );
    }
}

export default Modal;