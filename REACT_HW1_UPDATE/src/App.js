import './App.css';
import React, {Component} from 'react';
import Button from "./Components/Button/Button";
import Modal from "./Components/Modal/Modal";
import './ModalWindow.scss'


const HEADER = "Do you want to delete this file?";
const close = "X";
const CLOSE_BUTTON = Boolean(close);
const TEXT_FIRST_MODAL = "Once you delete this file, it won't be possible to undo this action. Are you sure you want to delete it?";
const TEXT_SECOND_MODAL = "Would you like to select this item? If yes, please do not forget to hit the button 'OK' to add it to your cart?";

const ACTION_BUTTON_STYLES = {
    width: "64px",
    padding: "8px",
    backgroundColor: "darkred",
    marginRight: "8px",
    marginLeft: "8px",
    fontSize: "16px",
    textAlign: "center",
    color: "white",
    border: "1px darkred",
    borderRadius: "4px",
};

class App extends Component {
    state = {
        open: false,
        textModal: false,
    };

    openModal = (e) => {
        this.setState({open: true});
        this.setState({textModal: e.target.dataset.text});
    };

    closeModal = () => {
        this.setState({open: false})
    };

    render() {
        const {open, textModal} = this.state;

        return (
            <>
                <Button text='Open first modal' backgroundColor={{background: "grey"}} onClick={this.openModal}
                        textModal={TEXT_FIRST_MODAL} />
                <Button text='Open second modal' backgroundColor={{background: "lightgrey"}} onClick={this.openModal}
                        textModal={TEXT_SECOND_MODAL} />
                <Modal open={open} closeModal={this.closeModal} header={HEADER} closeButton={CLOSE_BUTTON} close={close}
                       text={textModal}
                       actions={<div>
                           <Button styles={ACTION_BUTTON_STYLES} text="OK"/>
                           <Button styles={ACTION_BUTTON_STYLES} text='Cancel' onClick={this.closeModal}/>
                       </div>}/>
            </>
        );
    }
}

export default App;
