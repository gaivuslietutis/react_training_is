import React from 'react';
import GoodsCard from "../GoodsCard/GoodsCard";

function Main(props) {

    const {goods, onAdd, onAddFavorite, onRemoveFavorite, addFavProduct,favoriteItems } = props;

    const goodsItems = goods.map((good) => <GoodsCard key={good.id} good={good} onAdd={onAdd} onAddFavorite={onAddFavorite}
                                                      onRemoveFavorite={onRemoveFavorite}
                                                      addFavProduct={addFavProduct}
                                                      favoriteItems={favoriteItems}

    />);

    return (
        <>
            <div className="goods__wrapper">{goodsItems}</div>
        </>
    );
}


export default Main;