import React from 'react';
import Icon from "../Icon/Icon";
import {NavLink} from "react-router-dom";
import './navStyles.scss'


function Nav(props) {
    const {countCartItems} = props;

    return (
        <>
            <nav className="navigation-row">
                <Icon type="art" color="black"/>
                <div>
                    <NavLink className='linkInfo' activeClassName='linkInfo--active' to='/main'>Gallery
                    </NavLink> <NavLink className='linkInfo' activeClassName='linkInfo--active' to="/cart">
                    Cart{' '}
                    {countCartItems ? (
                        <button className='btn badge'>{countCartItems}</button>
                    ) : ('')}
                </NavLink> <NavLink className='linkInfo' activeClassName='linkInfo--active' to="/favorites">Favorites</NavLink>
                </div>
                <div> <a
                className='linkInfo sign' href="#/SignIn">SignIn</a></div>
            </nav>

        </>
    );

}

export default Nav;