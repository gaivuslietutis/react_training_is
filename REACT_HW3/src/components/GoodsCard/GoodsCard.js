import React, {useState} from 'react';
import Button from "../Button/Button";
import PropTypes from "prop-types"
import GoodsItem from "../GoodsItem/GoodsItem";
import Modal from "../Modal/Modal";
import {ACTION_BUTTON_STYLES} from '../Variables/modalStyles.js';
import {FaStar, FaRegStar} from 'react-icons/fa';

const HEADER = "Please confirm your selection?";
const close = "X";
const CLOSE_BUTTON = Boolean(close);
const TEXT_FIRST_MODAL = "Would you like to select this item? If yes, please do not forget to hit the button 'OK' to add it to your cart?";

function GoodsCard(props) {

    const [open, setOpen] = useState(false);
    const [textModal, setTextModal] = useState(false);

    const openModal = (e) => {
        const target = e.target.getAttribute("data-text");
        setOpen(true);
        setTextModal(target);
    };

    const closeModal = () => {
        setOpen(false)
    };

    const {good, buttonBuy, onAdd, favoriteItems, addFavProduct} = props;

    return (
        <>
            <div className="goods__card">
                <GoodsItem good={good}/>
                <Button text='Add to cart' onClick={openModal}
                        textModal={TEXT_FIRST_MODAL} good={good} buttonBuy={buttonBuy}/>
                {favoriteItems.includes(good.id) ? (<FaStar
                        onClick={() => addFavProduct(good)}
                        style={{color: "red", width: 36, height: 36}}
                    />
                ) : (<FaRegStar
                    onClick={() => addFavProduct(good)}
                    style={{color: "red", width: 36, height: 36}}
                />)}
            </div>
            <Modal open={open} closeModal={closeModal} header={HEADER} closeButton={CLOSE_BUTTON} close={close}
                   text={textModal}
                   actions={<div>
                       <button onClick={() => {
                           onAdd(good);
                           closeModal()
                       }} style={ACTION_BUTTON_STYLES}>OK
                       </button>
                       <button onClick={closeModal} style={ACTION_BUTTON_STYLES}>Cancel</button>
                   </div>}/>
        </>
    );
}

GoodsCard.propTypes = {
    good: PropTypes.object
};

export default GoodsCard;