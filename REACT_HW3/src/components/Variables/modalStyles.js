export const ACTION_BUTTON_STYLES = {
    width: "64px",
    padding: "8px",
    backgroundColor: "darkred",
    marginRight: "8px",
    marginLeft: "8px",
    fontSize: "16px",
    textAlign: "center",
    color: "white",
    border: "1px darkred",
    borderRadius: "4px",
};

export default ACTION_BUTTON_STYLES