import React from 'react';
import Nav from "../Nav/Nav";
import {PropTypes} from 'prop-types';
import Icon from '../Icon/Icon'

function Header(props) {

    const {navItems, countCartItems} = props;
    const titleOne = "Explore our art gallery...";

    return (
        <div className="header__wrapper">
            <Nav navItems={navItems} countCartItems={countCartItems}/>
            <h1 className="header__title"><Icon type='artGallery' color="darkgrey"/>{titleOne}</h1>

        </div>
    );
}

Header.propTypes = {
    navItems: PropTypes.array,
    countCartItems: PropTypes.number
};

export default Header;