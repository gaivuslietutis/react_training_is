import React from 'react';
import {Redirect, Route, Switch} from "react-router-dom";
import Favorites from "../pages/Favorites/Favorites";
import Main from "../pages/Main/Main";
import ShoppingCart from "../pages/ShoppingCart/ShoppingCart";
import Page404 from "../Page404/Page404";

function AppRoutes(props) {
    const {goods, onAdd, onRemove, cartItems, favoriteItems, addFavProduct} = props;

    return (
        <Switch>
            <Redirect exact from='/' to='/main'/>
            <Route exact path='/main' render={() => <Main goods={goods} onAdd={onAdd}
                                                          onRemove={onRemove}
                                                          addFavProduct={addFavProduct}
                                                          favoriteItems={favoriteItems}
            />}
            />
            <Route exact path='/favorites' render={() => <Favorites/>}/>
            <Route exact path='/cart'
                   render={() => <ShoppingCart cartItems={cartItems} onAdd={onAdd} onRemove={onRemove}/>}/>
            <Route exact path='*'><Page404/></Route>
        </Switch>
    );
}

export default AppRoutes;