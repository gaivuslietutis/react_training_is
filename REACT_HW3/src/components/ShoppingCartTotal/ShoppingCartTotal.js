import React from 'react';

function ShoppingCartTotal(props) {

    const {cartItems} = props;

    const itemsPrice = cartItems.reduce((a, c) => a + c.qty * c.price, 0);
    const taxPrice = itemsPrice * 0.18;
    const shippingPrice = itemsPrice > 2000 ? 0 : 50;
    const totalPrice = itemsPrice + taxPrice + shippingPrice;

    return (
        <div>
            {cartItems.length !== 0 && (
                <>
                    <hr></hr>
                    <div className='row'>
                        <div className='col-2'>Items Price</div>
                        <div className='col-1 text-right'>${itemsPrice.toFixed(2)}</div>
                    </div>
                    <div className='row'>
                        <div className='col-2'>Tax Price</div>
                        <div className='col-1 text-right'>${taxPrice.toFixed(2)}</div>
                    </div>
                    <div className='row'>
                        <div className='col-2'>Shipping Price</div>
                        <div className='col-1 text-right'>${shippingPrice.toFixed(2)}</div>
                    </div>
                    <div className='row'>
                        <div className='col-2'>
                            <strong>TOTAL</strong>
                        </div>
                        <div className='col-1 text-right'>
                            <strong>${totalPrice.toFixed(2)}</strong>
                        </div>
                    </div>
                    <hr/>
                    <div className="row">
                        <button className='btn' onClick={() => {
                            alert("Implement Checkout")
                        }}>Checkout
                        </button>
                    </div>
                    <hr></hr>
                </>
            )}
        </div>
    );
}

export default ShoppingCartTotal;