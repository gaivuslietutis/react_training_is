import React, {Component} from 'react';
import ContactIconsBlock from "../ContactIconsBlock/ContactIconsBlock";

const descriptionBody = "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Lorem ipsum dolor sit amet, consectetur adipisicing elit.\n" +
    "A beatae doloribus, ducimus enim error est eum.A beatae doloribus, ducimus enim error es. A beatae doloribus, ducimus " +
    "enim error est eum.A beatae doloribus, ducimus enim error est eum. A beatae doloribus, ducimus enim error est eum.A beatae doloribus, ducimus enim error es." +
    " A beatae doloribus, ducimus \n"


class BodyHead extends Component {
    render() {
        return (
            <div className="bodyhead__wrapper">
                <ContactIconsBlock/>
                <h1 className="body__title">Art gallery...</h1>
                <p className="body__desription">{descriptionBody}</p>
            </div>
        );
    }
}

export default BodyHead;