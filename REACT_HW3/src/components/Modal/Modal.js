import React from 'react';

function Modal (props) {

        const {header, closeButton, close, text, actions, open, closeModal} = props;

        if (!open) return null;

        return (
            <>
                <div className='modal-overlay' onClick={closeModal}/>
                <div className='modal-window'>
                    <div className='modal-window__header'>
                        <div>{header}</div>
                        {closeButton && <span onClick={closeModal}>{close}</span>}</div>
                    <div className='modal-window__body'>
                        <div>{text}</div>
                        <div className='action-wrapper'>
                            {actions}</div>
                    </div>
                </div>
            </>
        );

}

export default Modal;

