import React from 'react';
import GoodsCard from "../../GoodsCard/GoodsCard";


function Main(props) {

    const {goods, onAdd, onAddFavorite, onRemoveFavorite, favoriteItems, addFavProduct} = props;

    const goodsItems = goods.map(g => <GoodsCard key={g.id} good={g} onAdd={onAdd}
                                                 onAddFavorite={onAddFavorite}
                                                 onRemoveFavorite={onRemoveFavorite}
                                                 addFavProduct={addFavProduct}
                                                 favoriteItems={favoriteItems}
    />);

    return (
        <>
            <div className="goods__wrapper">{goodsItems}</div>
        </>
    );
}

export default Main;