import React, {useState} from 'react';
import './shoppingCartStyles.scss'
import {ACTION_BUTTON_STYLES} from '../../Variables/modalStyles.js'
import Modal from "../../Modal/Modal";
import ShopCartItem from "../../ShopCartItem/ShopCartItem";
import ShoppingCartTotal from "../../ShoppingCartTotal/ShoppingCartTotal";

const HEADER = "Please confirm your selection?";
const close = "X";
const CLOSE_BUTTON = Boolean(close);
const TEXT_CART_MODAL = "Are you sure to delete this item from your cart?";


function ShoppingCart(props) {
    const {cartItems, onRemove} = props;
    const [open, setOpen] = useState(false);

    const openModal = (e) => {
        setOpen(true);
    };

    const closeModal = () => {
        setOpen(false)
    };

    const cartItemsList = cartItems.map(item => <div key={item.id}><ShopCartItem shopItem={item} openModal={openModal}/>
        <Modal open={open} closeModal={closeModal} header={HEADER} closeButton={CLOSE_BUTTON}
               close={close}
               text={TEXT_CART_MODAL}
               actions={<div>
                   <button onClick={() => {
                       onRemove(item);
                       closeModal()
                   }} style={ACTION_BUTTON_STYLES}>OK
                   </button>
                   <button onClick={closeModal} style={ACTION_BUTTON_STYLES}>Cancel</button>
               </div>}/>
    </div>);


    return (
        <>
            <div className='block row center' >
                <h2>Shopping Cart</h2>
                <div >
                    {cartItemsList}
                </div>
                <div>
                <ShoppingCartTotal cartItems={cartItems}/>
                </div>
                <div >
                    {cartItems.length === 0 && <div>Cart is empty</div>}
                </div>
            </div>

        </>
    );
}

export default ShoppingCart;