import React from 'react';
import './favoritesStyle.scss'

function Favorites(props) {

    let favList = [{}];
    const getArray = JSON.parse(localStorage.getItem('FAVORITES LIST') || 0);
    for (let i = 0; i < getArray.length; i++) {
        let x = getArray[i];
        favList[i] = JSON.parse(localStorage.getItem("FAVORITES PRODUCT" + [x] || ""))
    }

    return (
        <div className='block row center'>
            <h3>YOUR FAVORITES</h3>
            <div className='row center'>
                <div>{Object.keys(favList[0]).length === 0 ? <div>Add Favorites</div>:
                    <div className='row center col-2'>
                        {favList.map((item, index) => (
                            <div key={index}><img className='medium' src={item.image} alt='img'/></div>))}
                    </div>
                }</div>
            </div>
        </div>
    );
}

export default Favorites;