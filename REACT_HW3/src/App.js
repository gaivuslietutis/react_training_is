import './App.css';
import "./Styles.scss";
import "./ModalWindow.scss"
import React, {useEffect, useState} from 'react';
import Header from "./components/Header/Header";
import Footer from "./components/Footer/Footer";
import BodyHead from "./components/BodyHead/BodyHead";
import axios from "axios";
import AppRoutes from "./components/routes/AppRoutes";

function App() {

    const [goods, setGoods] = useState([]);
    const [cartItems, setCartItems] = useState([]);
    const [favoriteItems, setFavoriteItems] = useState([]);

    useEffect(() => {
        axios("data_goods.json")
            .then(res => {
                setGoods(res.data);
            });
    }, []);


    const getArray = JSON.parse(localStorage.getItem('FAVORITES LIST') || 0);

    useEffect(() => {
        if (getArray !== 0) {
            setFavoriteItems([...getArray])
        }
    }, []);

    const onAdd = (product) => {
        const exist = cartItems.find(x => x.id === product.id);
        if (exist) {
            setCartItems(cartItems.map(x =>
                x.id === product.id ? {...exist, qty: exist.qty + 1} : x))
        } else {
            setCartItems([...cartItems, {...product, qty: 1}])
        }
        localStorage.setItem('SHOP PRODUCT' + (product.id), JSON.stringify(product))
    };

    const onRemove = (product) => {
        const exist = cartItems.find(x => x.id === product.id);
        if (exist) {
            setCartItems(cartItems.filter(x => x.id !== product.id))
        }
        localStorage.removeItem('SHOP PRODUCT' + (product.id))
    };

    const addFavProduct = (product) => {

        let array = favoriteItems;
        let addArray = true;
        array.map((item, key) => {
            if (item === product.id) {
                array.splice(key, 1);
                addArray = false;
            }
        });
        if (addArray) {
            array.push(product.id);
        }
        setFavoriteItems([...array]);

        localStorage.setItem("FAVORITES LIST", JSON.stringify(favoriteItems));
        let storage = localStorage.getItem('FAVORITES PRODUCT' + product.id || '0');
        if (storage === null) {
            localStorage.setItem('FAVORITES PRODUCT' + (product.id), JSON.stringify(product))
        } else {
            localStorage.removeItem('FAVORITES PRODUCT' + (product.id))
        }
    };

    return (<>
            <Header countCartItems={cartItems.length}/>
            <BodyHead/>
            <AppRoutes goods={goods} onAdd={onAdd}
                       onRemove={onRemove}
                       cartItems={cartItems}
                       addFavProduct={addFavProduct}
                       favoriteItems={favoriteItems}
            />
            <Footer/>
        </>
    );
}


export default App;
