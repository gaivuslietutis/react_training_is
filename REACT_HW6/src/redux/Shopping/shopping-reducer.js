import * as actionTypes from './shopping-types'

const INITIAL_STATE = {
    cartItems: [],
    open: false,
    currentItems: null,
    favoriteItems: [],
    shoppingOrder: {}
};

const shopReducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {

        case actionTypes.ADD_TO_CART:
            const item = action.payload;
            const exist = state.cartItems.find(x => x.id === action.payload.id);
            return {
                ...state,
                cartItems: exist ?
                    state.cartItems.map(x =>
                        x.id === item.id ? {...exist, qty: exist.qty + 1} : x)
                    :
                    [...state.cartItems, {...item, qty: 1}],
            };

        case actionTypes.REMOVE_FROM_CART:
            return {
                ...state,
                cartItems: state.cartItems.filter(item => item.id !== action.payload.id),
            };

        case actionTypes.TOGGLE_FAVORITES: {
            const favorite = action.payload;
            console.log(favorite);
            if (state.favoriteItems.includes(favorite)) {
                return {...state, favoriteItems: state.favoriteItems.filter(e => e !== favorite)}
            } else {
                return {...state, favoriteItems: [...state.favoriteItems, favorite]}
            }
        }

        case actionTypes.SET_ORDER: {
            return {...state, shoppingOrder: {...state.shoppingOrder, shoppingOrder: action.payload}}
        }

        default:
            return state

    }
};

export default shopReducer;