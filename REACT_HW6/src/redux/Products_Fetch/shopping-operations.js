import axios from "axios";


export const getGoods=()=>(dispatch)=>{
    axios('data_goods.json')
        .then(res => {
               dispatch({type:"SET_GOODS", payload:res.data})
        })
};

