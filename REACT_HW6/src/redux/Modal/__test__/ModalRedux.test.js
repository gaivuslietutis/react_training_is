import * as actionTypes from '../modal-types';
import reducer from '../modal-reducer'
import {MODAL_OPEN} from "../modal-types";


describe("modal window open close operations", () => {
    const INITIAL_STATE_OPEN = {
        open: false
    };

    const actionOpen = {type:"MODAL_OPEN", payload: true};

    test('testing modal reducer open', () => {
        const newState = reducer(INITIAL_STATE_OPEN, actionOpen);
        expect(newState.open).toBe(true)

    });
    const INITIAL_STATE_CLOSE = {
        open: true
    };
    const action = {type: "MODAL_CLOSE", payload: false};

    test('testing modal reducer open', () => {
        const newState = reducer(INITIAL_STATE_CLOSE, action);
        expect(newState.open).toBe(false)

    });
});