import * as actionTypes from './modal-types';


const INITIAL_STATE = {
    open: false,
};

export const modalReducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {

        case actionTypes.MODAL_OPEN: {
            return {...state, open:true}
        }

        case actionTypes.MODAL_CLOSE: {
            return {...state, open:false}
        }

        default:
            return state
    }
};


export default modalReducer;

