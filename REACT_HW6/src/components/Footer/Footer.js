import React, {Component} from 'react';

class Footer extends Component {
    render() {
        return (
            <div className="footer__wrapper">
                <h2>Copyright<span>&copy;</span>2021</h2>
            </div>
        );
    }
}

export default Footer;