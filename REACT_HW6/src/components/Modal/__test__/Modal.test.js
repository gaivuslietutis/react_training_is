import { render, screen } from '@testing-library/react';
import Modal from '../Modal';
import React from "react";
import {MODAL_CLOSE} from "../../../redux/Modal/modal-types";

describe("Modal window structure", () => {
    test('should be classes ', () => {
        render(
            <Modal/>
        );
        const modalOverlayClass = document.getElementsByClassName('modal-overlay');
        expect(modalOverlayClass).toBe(modalOverlayClass)
    });

    test('should be classes modal; ', () => {
        render(
            <Modal/>
        );
        const modalWindowClass = document.getElementsByClassName('modal-window');
        expect(modalWindowClass).toBe(modalWindowClass)
    });


    test('should be div ', () => {
        render(
            <Modal/>
        );
        const modalDiv = document.getElementsByTagName('div');
        expect(modalDiv).toBe(modalDiv)
    });

});

it('renders welcome message', () => {
    render(<Modal />);
    expect(screen.getByText('Learn React')).toBeInTheDocument();
});

describe('Modal Snapshot tests', () => {
    test('Simple Modal snapshot test', () => {
        const { container } = render(<Modal />);
        expect(container).toMatchSnapshot();
    });
});
