import React from 'react';
import Icon from "../Icon/Icon";
import {NavLink} from "react-router-dom";
import './navStyles.scss'
import {connect} from "react-redux";


function Nav(props) {

    const {cartItems, user} = props;
    const countCartItems = cartItems.length;

    const isAuth = !!user;

    return (
        <>
            <nav className="navigation-row">
                <Icon type="art" color="black"/>
                <div>
                    <NavLink className='linkInfo' activeClassName='linkInfo--active' to='/main'>Gallery</NavLink>
                    <NavLink className='linkInfo' activeClassName='linkInfo--active' to="/cart">
                    Cart{' '}
                    {countCartItems ? (
                        <button className='btn badge'>{countCartItems}</button>
                    ) : ('')}</NavLink>
                    <NavLink className='linkInfo' activeClassName='linkInfo--active' to="/favorites">Favorites</NavLink>
                </div>
                <div><NavLink className='linkInfo sign' to='/login'>{!isAuth ? 'SignIn' : 'You are signed'}</NavLink></div>
            </nav>
        </>
    );

}

const mapStateToProps = (state) => {
    return {
        cartItems: state.shop.cartItems,
        user: state.users.user.data
    }
}

export default connect(mapStateToProps)(Nav);