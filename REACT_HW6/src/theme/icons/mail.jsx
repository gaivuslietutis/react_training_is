import React from "react";

export function mail(color='black', filled=false, onClick){
    return (
        <svg xmlns="http://www.w3.org/2000/svg"
             viewBox="0 0 24 24" height='36' width='36' fill="none" stroke="currentColor"
             strokeWidth="2" strokeLinecap="round" strokeLinejoin="round"
             className="feather feather-mail">
            <path d="M4 4h16c1.1 0 2 .9 2 2v12c0 1.1-.9 2-2 2H4c-1.1 0-2-.9-2-2V6c0-1.1.9-2 2-2z"
                  stroke={color}
                  fill={filled ? color: "none" }
                  onClick={onClick}

            />
            <polyline points="22,6 12,13 2,6"></polyline>
        </svg>

    )


}



