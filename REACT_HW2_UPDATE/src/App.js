import './App.css';
import "./Styles.scss";
import "./ModalWindow.scss"
import React, {Component} from 'react';
import Header from "./components/Header/Header";
import Footer from "./components/Footer/Footer";
import BodyHead from "./components/BodyHead/BodyHead";
import Goods from "./components/Goods/Goods";

class App extends Component {

  render() {
      return (<>
          <Header/>
          <BodyHead/>
          <Goods/>
          <Footer/>
          </>
    );
  }
}


export default App;
