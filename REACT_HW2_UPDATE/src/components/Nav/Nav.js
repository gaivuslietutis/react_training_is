import React, {Component} from 'react';
import Icon from "../Icon/Icon";

const navArray=[
    {id: 1, title:"Menu", link:"abc.www"},
    {id: 2, title:"Contacts", link:"src.com"},
    {id: 3, title:"Information", link: "fff.com"},
    {id: 4, title:"More", link:"https://www.google.com"},
];

const navItems=navArray.map(l=><li key={l.id} className="linkItem"><a href={l.link}>{l.title}</a></li>);

class Nav extends Component {

    render() {
        return (
            <>
            <div className="navigation__wrapper">
                <Icon type="art" color="black"/>
                {navItems}
            </div>
            </>
        );
    }
}

export default Nav;