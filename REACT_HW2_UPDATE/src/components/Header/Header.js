import React, {Component} from 'react';
import Nav from "../Nav/Nav";
import PropTypes from "prop-types"
import Icon from "../../../../react-hw2-updated/src/components/Icon/Icon";

class Header extends Component {


    render() {
        const{navItems}=this.props;
        const titleOne="Explore our art gallery...";

        return (
            <div className="header__wrapper" >
               <Nav navItems={navItems}/>
               <h1 className="header__title"><Icon type='artGallery' color="darkgrey"/>{titleOne}</h1>

            </div>
        );
    }
}

Header.defaultProps={
    title:"Default Title",
    navItems:PropTypes.array.isRequired
};

export default Header;