import React, {Component} from 'react';

class GoodsItem extends Component {
    render() {
        const {good} = this.props;

        return (
            <div className='goods__item'>
                <h4>{good.title}</h4>
                <div>PRICE: {good.price} EUR</div>
                <img src={good.image} alt="img"/>
                <div>ARTICLE:{good.article}</div>
                <div>COLOR:{good.color}</div>
            </div>
        );
    }
}

export default GoodsItem;