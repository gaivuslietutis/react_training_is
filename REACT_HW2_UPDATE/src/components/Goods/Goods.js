import React, {Component} from 'react';
import GoodsCard from "../GoodsCard/GoodsCard";
import axios from "axios";

class Goods extends Component {
    state = {
        goods: [],
    };

    componentDidMount() {
        axios("data_goods.json")
            .then(res => {
                this.setState({goods: res.data});
            });
    }

    render() {
        const {goods} = this.state;

        const goodsItems=goods.map(g=><GoodsCard key={g.id} good={g}/>);
        return (
            <>
                <div className="goods__wrapper">{goodsItems}</div>
            </>
        );
    }
}

export default Goods;