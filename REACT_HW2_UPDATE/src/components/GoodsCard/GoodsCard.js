import React, {Component} from 'react';
import Button from "../Button/Button";
import PropTypes from "prop-types"
import Modal from "../Modal/Modal";
import GoodsItem from "../GoodsItem/GoodsItem";
import Icon from "../Icon/Icon";

const HEADER = "Please confirm your selection?";
const close = "X";
const CLOSE_BUTTON = Boolean(close);
const TEXT_FIRST_MODAL = "Would you like to select this item? If yes, please do not forget to hit the button 'OK' to add it to your cart?";

const ACTION_BUTTON_STYLES = {
    width: "64px",
    padding: "8px",
    backgroundColor: "darkred",
    marginRight: "8px",
    marginLeft: "8px",
    fontSize: "16px",
    textAlign: "center",
    color: "white",
    border: "1px darkred",
    borderRadius: "4px",
};

class GoodsCard extends Component {
    state = {
        open: false,
        textModal: false,
        isFavorite: false

    };

    openModal = (e) => {
        this.setState({open: true});
        this.setState({textModal: e.target.dataset.text});
    };

    closeModal = () => {
        this.setState({open: false})
    };

    addToCart = (e) => {
        const {good} = this.props;
        console.log(good.title);
        if (e.target) {
            localStorage.setItem("chosenItem", JSON.stringify(good))
        }
        this.setState({open: false})
    };

    doAsFavorite = () => {
        this.setState({isFavorite: true})
    };

    render() {
        const {good, buttonBuy} = this.props;
        const {open, textModal, isFavorite} = this.state;


        return (
            <>
                <div className="goods__card">
                    <GoodsItem good={good}/>
                    <Button text='Add to cart' onClick={this.openModal}
                            textModal={TEXT_FIRST_MODAL} good={good} buttonBuy={buttonBuy}/>
                    <Icon type='star' color="red" filled={isFavorite} onClick={this.doAsFavorite}/>
                </div>
                <Modal open={open} closeModal={this.closeModal} header={HEADER} closeButton={CLOSE_BUTTON} close={close}
                       text={textModal}
                       actions={<div>
                           <Button styles={ACTION_BUTTON_STYLES} title="Buy" text="OK" onClick={this.addToCart}/>
                           <Button styles={ACTION_BUTTON_STYLES} text='Cancel' onClick={this.closeModal}/>
                       </div>}/>
            </>
        );
    }
}

GoodsCard.propTypes = {
    good: PropTypes.object
};

export default GoodsCard;