import React, {Component, Fragment} from 'react';

class Modal extends Component {
    render() {
        const {header, closeButton, close, text, actions, open, closeModal} = this.props;

        if (!open) return null;

        return (
            <>
                <div className='modal-overlay' onClick={closeModal}/>
                <div className='modal-window'>
                    <div className='modal-window__header'>
                        <div>{header}</div>
                        {closeButton && <span onClick={closeModal}>{close}</span>}</div>
                    <div className='modal-window__body'>
                        <div>{text}</div>
                        <div className='action-wrapper'>
                            {actions}</div>
                    </div>
                </div>
            </>
        );
    }
}

export default Modal;






// import React, {Component} from 'react';
//
// class Modal extends Component {
//
//     render() {
//         const {header, text, actions, closeModal} = this.props;
//
//         return (
//             <div className="overlay" onClick={closeModal}>
//                 <div className="modal">
//                     <div className="modalHeader">
//                         <p className="modalTitle">{header}</p>
//                         <button className="modalButton__close" onClick={closeModal}>X
//                         </button>
//                     </div>
//                     <div className="modalBody">
//                         <div>{text}</div>
//                         {actions}
//                     </div>
//                 </div>
//             </div>
//         );
//     }
// }
//
// export default Modal;