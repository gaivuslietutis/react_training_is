import React from 'react';
import * as icons from '../../theme/icons';
import './iconstyles.scss'

function Icon(props) {
    const { type, color, filled, onClick } = props;


    const iconJsx = icons[type];

    if (!iconJsx) {
        return null;
    }

    return (
        <span className='icon-style' onClick={onClick}>{iconJsx(color, filled)}</span>
    )
}

export default Icon
