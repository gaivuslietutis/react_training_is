import React, {Component} from 'react';


class Button extends Component {
    
    render() {
        const {text, onClick, textModal, styles} = this.props;

        return (
            <>
                <button className="buttonBuy"  style={{...styles}} onClick={onClick} data-text={textModal}>{text}</button>
            </>
        );
    }
}

export default Button;